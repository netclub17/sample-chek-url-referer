<?php
session_start();

$ori_link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
$referer_link = isset($ori_link) ? parse_url($ori_link, PHP_URL_HOST) : '';

$protocol = (!empty($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
$compare_link = $protocol . $_SERVER['HTTP_HOST']; // compare in your url my host

if (strcmp($referer_link, $compare_link) != 0) { // check not match
    $_SESSION['last_referer'] = !empty($ori_link) ? '<strong>คุณจิ้มลิงค์มาจาก >>> </strong> <span style="color:blue;">' . $ori_link . '</span>' : '';
}

if (!empty($_SESSION['last_referer'])) {
    echo  $_SESSION['last_referer'];
    unset($_SESSION['last_referer']);
} else {
    echo '<h1>เฮ้ย เข้าตรงๆ เลยหรอว่ะแสรด !!</h1>';
}
